module "networking" {
  source       = "./modules/Networking"
  tag_resource = var.tag_resource
}

module "iam" {
  source       = "./modules/IAM"
  tag_resource = var.tag_resource
  secret_arn   = module.secrets.aws_secret_arn
}

module "security" {
  source       = "./modules/Security"
  tag_resource = var.tag_resource

  vpc_id = module.networking.aws_vpc_id
}

module "secrets" {
  source        = "./modules/Secrets"
  tag_resource  = var.tag_resource
  vpc_id        = module.networking.aws_vpc_id
  secret_string = var.gitlab_token
}

module "ec2-loadbalance" {
  source       = "./modules/Ec2AndALB"
  tag_resource = var.tag_resource
  prefix       = var.prefix

  vpc_id                    = module.networking.aws_vpc_id
  iam_instance_profile_name = module.iam.iam_instance_profile_name
  subnet_ids                = module.networking.aws_subnet_ids
  sg_lb_id                  = module.security.sg_lb_id
  sg_instance_id            = module.security.sg_instance_id
  asg_desired               = var.asg_desired
  asg_max                   = var.asg_max

}

module "ecs" {
  source       = "./modules/ECS"
  tag_resource = var.tag_resource
  prefix       = var.prefix

  desired_count                     = var.desired_tasks
  ecs_task_definition_file_location = var.ecs_task_definition_file_location
  container_name                    = var.ecs_container_name
  repository_image                  = var.ecs_container_repository_image

  secret_arn            = module.secrets.aws_secret_arn
  iam_role_task_def_arn = module.iam.iam_role_ecs_task_def_arn
  iam_role_ecs_name     = module.iam.iam_role_ecs_name

  alb_tg_ecs_id = module.ec2-loadbalance.alb_tg_ecs_id
}

