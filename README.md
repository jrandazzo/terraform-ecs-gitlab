# Terraform: Deploy ECS Environment to Pull from ECR or GitLab Registry
### Overview

### Instructions
1. Clone this repository to a local machine.
2. Read the [Terraform Quickstart](https://learn.hashicorp.com/terraform/getting-started/install.html) to download Terraform CLI and learn about the basic commands.
3. Insert your AWS credentials by a [documented method](https://www.terraform.io/docs/providers/aws/index.html#authentication) so Terraform can construct AWS resources.
4. Determine if ECS is pulling from ECR or GitLab Registry -
   1. Using ECR:
      1. Assuming a ECR registry is created and a container has been pushed, update `ecs_container_repository_image` to the ECR URI specified in the AWS console.
5. In the root level of the project, run the following commands in the terminal.
   ```bash
	# Download required providers and resources
	terraform init
	
	# Review output of planned resources to generate and ensure no errors
	terraform plan
	
	# Review output of planned resources. It will asking "Do you want to perform these actions?". Reply with `yes` in the terminal.
	terraform apply
	 ```
	The AWS resources will now be created which takes about ~2 minutes.

6. To retrieve the ALB address to view the ECS service, run the following command 
   ```bash
	 terraform output lb_address
	 # This returns a value like tf-alb-ecs-1338829522.us-east-2.elb.amazonaws.com
	 ```

7. When you are all finished with the demo, please run the following command to tear down AWS resources so charges don't incur.
   ```bash
		# Destroy the AWS resources. It will asking "Do you want to perform these actions?". Reply with `yes` in the terminal.
		terraform destroy
	 ```

### Disclaimer
Th generated AWS resources is for demo purposes only. This is not intended for production-level environments.




