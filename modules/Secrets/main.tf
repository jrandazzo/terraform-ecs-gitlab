
resource "random_id" "secret" {
  keepers = {
    # Generate a new id each time we create a new vpc (New demo)
    vpc_id = "${var.vpc_id}"
  }

  byte_length = 8
}

resource "aws_secretsmanager_secret" "secret" {
  name = "tf-secret-${random_id.secret.hex}"
}
resource "aws_secretsmanager_secret_version" "secret_value" {
  secret_id     = aws_secretsmanager_secret.secret.id
  secret_string = jsonencode(var.secret_string)
}