output "aws_vpc_id" {
  value = aws_vpc.main.id
}

output "aws_subnet_ids" {
  value = ["${aws_subnet.main.0.id}", "${aws_subnet.main.1.id}"]
}
