variable "tag_resource" {
}

variable "prefix" {

}

variable "ecs_task_definition_file_location" {
  default = "./task_definition.json"
}

variable "desired_count" {
}


variable "container_name" {
}

variable "repository_image" {
}

variable "secret_arn" {
}

variable "iam_role_task_def_arn" {
}

variable "iam_role_ecs_name" {
}

variable "alb_tg_ecs_id" {
}


