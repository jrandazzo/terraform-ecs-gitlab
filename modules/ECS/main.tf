
resource "aws_ecs_cluster" "main" {
  name = "${var.prefix}-tf-ecs-cluster"
}

resource "aws_ecs_task_definition" "task_definition" {
  family = "${var.prefix}-tf-task-def"
  container_definitions = templatefile("${var.ecs_task_definition_file_location}", {
    image_url           = "${var.repository_image}"
    container_name      = "${var.container_name}"
    registry_policy_arn = "${var.secret_arn}"
  })
  requires_compatibilities = ["EC2"]
}

resource "aws_ecs_service" "service" {
  name            = "${var.prefix}-tf-ecs-service"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.task_definition.family
  desired_count   = var.desired_count
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent = 200
  iam_role        = var.iam_role_ecs_name

  load_balancer {
    target_group_arn = var.alb_tg_ecs_id
    container_name   = var.container_name
    container_port   = "5000"
  }
  #ALB must be created before generating ECS service
  #See output in Ec2AndALB for depends_on
  #https://github.com/terraform-providers/terraform-provider-aws/issues/11723

}

