output "iam_role_ecs_task_def_arn" {
  value = aws_iam_role.ecs_tasks.arn
}

output "iam_role_ecs_name" {
  value = aws_iam_role.ecs.name
}

output "iam_instance_profile_name" {
  value = aws_iam_instance_profile.app.name
}
