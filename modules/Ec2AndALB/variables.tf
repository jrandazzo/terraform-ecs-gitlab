variable "tag_resource" {
  default = "tf-ecs-demo"
}

variable "prefix" {

}

variable "vpc_id" {
}

variable "iam_instance_profile_name" {
}

variable "subnet_ids" {
}

variable "sg_lb_id" {
}

variable "sg_instance_id" {
}
variable "asg_min" {
  default = 0
}

variable "asg_max" {
  default = 1
}

variable "asg_desired" {
  default = 1
}