resource "aws_launch_template" "app" {
  name_prefix   = "${var.prefix}-tf-template-ecs"
  image_id      = "ami-05e7fa5a3b6085a75"
  instance_type = "t2.micro"
  user_data = base64encode(templatefile("${path.module}/ec2_user_data.tpl", {
    ecs_cluster_name = "${var.prefix}-tf-ecs-cluster"
  }))
  network_interfaces {
    associate_public_ip_address = true
    security_groups             = ["${var.sg_instance_id}"]
  }
  iam_instance_profile {
    name = var.iam_instance_profile_name
  }

}

resource "aws_autoscaling_group" "asg" {
  name                = "${var.prefix}-tf-asg-ecs"
  vpc_zone_identifier = var.subnet_ids
  min_size            = var.asg_min
  max_size            = var.asg_max
  desired_capacity    = var.asg_desired

  launch_template {
    id      = aws_launch_template.app.id
    version = "$Latest"
  }


  tag {
    key                 = "Name"
    value               = "${var.prefix}-tf-ecs-instance-demo-by-asg"
    propagate_at_launch = true
  }
}


resource "aws_alb_target_group" "tg_ecs" {
  name     = "${var.prefix}-tf-ecs-tg"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  tags = {
    Name = "${var.tag_resource}"
  }
}

resource "aws_alb" "alb" {
  name            = "${var.prefix}-tf-alb-ecs"
  subnets         = var.subnet_ids
  security_groups = ["${var.sg_lb_id}"]

  tags = {
    Name = "${var.tag_resource}"
  }
}

resource "aws_alb_listener" "ecs" {
  load_balancer_arn = aws_alb.alb.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.tg_ecs.id
    type             = "forward"
  }
}