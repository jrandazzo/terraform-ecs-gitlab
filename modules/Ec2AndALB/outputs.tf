output "alb_tg_ecs_id" {
  value = aws_alb_target_group.tg_ecs.id


  #ALB must be created before use of variables above
  #https://github.com/terraform-providers/terraform-provider-aws/issues/11723
  depends_on = ["aws_alb_listener.ecs"]
}

output "lb_address" {
  value = aws_alb.alb.dns_name
}
