output "sg_lb_id" {
  value = aws_security_group.lb_sg.id
}

output "sg_instance_id" {
  value = aws_security_group.instance_sg.id
}