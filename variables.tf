variable "aws_region" {
  default = "us-east-1"
}

variable "tag_resource" {
  default = "tf-ecs-demo"
}

variable "prefix" {
  default = "prefix"
}

variable "ecs_container_name" {
  default = "container-inserted"
}

variable "ecs_container_repository_image" {
  default = "registry.gitlab.com/group/project/image-name:tag"
}

variable "ecs_task_definition_file_location" {
  default = "custom_task_definition.json"
}

variable "asg_max" {
  default = 1
}

variable "asg_desired" {
  default = 1
}

variable "desired_tasks" {
  default = 1
}

variable "gitlab_token" {
  default = {
    password = "insert-token"
    username = "insert-username"
  }

  type = map(string)
}

